package uuidgenerator.handler

import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException
import org.eclipse.ui.IWorkbenchWindow
import org.eclipse.ui.handlers.HandlerUtil
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.emf.ecore.util.EcoreUtil
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection

class GenerationHandler extends AbstractHandler {
	
	
	override Object execute(ExecutionEvent event) throws ExecutionException {
		
		val uuid = EcoreUtil.generateUUID
		val selection = new StringSelection(uuid.toString)
		
		Toolkit.defaultToolkit.systemClipboard => [
			setContents(selection, selection)
		]
		
		var IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event)
		MessageDialog.openInformation(window.getShell(), "UUIDGenerator", 
		"The following UUID was copied to the clipboard:\n" + uuid.toString)
		return null
	}
}
